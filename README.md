# Speaking voice pitch estimation

Implementation of a speaking voice pitch estimator for ESP32 and Arduino.

This project is intended as a building block for technologies aimed at assisting people with speech and voice impairments. 
For example, a large part of people suffering from Parkinson's Disease (PD) have reduced loudness and a monotone, breathy voice.
As a result of reduced speech intelligibility, people with PD report they are less likely to participate in conversation, or have confidence in social settings than healthy individuals in their age group, thus speech disorders can progressively diminish quality of life [Parkinson's Foundation] (https://www.parkinson.org/pd-library/fact-sheets/Speech-Therapy).
By speaking louder and at the same preventing the pitch to raise, intelligibility immediately improves, and based on this insight, speech therapies and accompanying smartphone training applications have been introduced [Voice Trainer] (https://www.radboudumc.nl/en/afdelingen/revalidatie/voice-trainer/voice-trainer). 

Pitch estimation has received a great deal of attention over the past decades, due to its central importance in several domains, ranging from music information retrieval to speech analysis. Contemporary pitch estimators frequently employ deep learning methods such as Self-Supervised Pitch Estimation (Tagliasacchi, Google Research [2019] (https://ai.googleblog.com/2019/11/spice-self-supervised-pitch-estimation.html)).

In this project we follow an approach based on traditional signal processing pipelines for sake of simplicity. 
Typically, simple pipelines have a lower computational complexity compared to neural networks and require less training data, thus increasing the probability of succesful implementation on a low-power microcontroller.

In order to increase robustness, e.g. voice activity detection and improved noise immunity, we combine engineered features and traditional Machine Learning (ML), as proposed in (Drugman et al. in IEEE signal processing letters, [2018] (https://ieeexplore.ieee.org/document/8481420)). According to this work, among a list of time, spectral end cepstral domain acoustic features, the discriminative power of spectral features is highest for voicing detection, Summation of the Speech Harmonics (SSH and SSH)

This could be a possible test set: [Louis Farrakhan Blasts Mike Wallace - Harmonizator] (https://www.youtube.com/watch?v=exFfxfJNLaM)


## Hardware
Tested on Tiny Pico board and SPH0645LM4H I2s MEMS microphone break-out board.
By default, the SPH0645LM4H channel select pin is low, so that it will transmit on the left channel mono


<!-- # Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!).  Thank you to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README
Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project. -->

## License
2-Clause BSD License ("Simplified BSD License" or "FreeBSD License")

## Sources
<ul>
<li>https://github.com/atomic14/esp32_audio</li>
<li>https://github.com/flrs/HampelFilter</li>
<li>http://www.robinscheibler.org/2017/12/12/esp32-fft.html</li>
<li>https://github.com/yash-sanghvi/ESP32/issues/1</li>
<li> https://docs.espressif.com/projects/esp-idf/en/latest/esp32/api-reference/peripherals/i2s.html</li>
</ul>

## Project status
In progress
<!-- If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers. -->
