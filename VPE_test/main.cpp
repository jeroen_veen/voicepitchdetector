#include <iostream>
#include <string>
#include "../VPE/vpe.h"
#include "AudioFile.h"
#include "unity.h"
#include "test_fft.h"
#include <QStringList>
#include <QFile>
#include <QTextStream>

constexpr const char *inFileName = "C:\\Users\\scbry\\OneDrive - HAN\\APPS\\Jeroen Veen\\data\\RDH_VL\\16000_vl00001.wav";
constexpr const char *outFileName = "C:\\Users\\scbry\\OneDrive - HAN\\APPS\\Jeroen Veen\\data\\RDH_VL\\16000_vl00001.wav.qt.csv";

using namespace std;

// This function is called before each test
void setUp(void)
{
}

// This function is called after each test
void tearDown(void)
{
}

int unity_main(void)
{
    UNITY_BEGIN();
    RUN_TEST(test_rfft);
    return UNITY_END();
}

int main()
{
    AudioFile<float> audioData;
    VPE vpe = VPE();
    float freq, mag;
    QStringList strList;

    unity_main();

    bool loadedOK = audioData.load(inFileName);
    assert (loadedOK);

    std::cout << "Bit Depth: " << audioData.getBitDepth() << std::endl;
    std::cout << "Sample Rate: " << audioData.getSampleRate() << std::endl;
    std::cout << "Num Channels: " << audioData.getNumChannels() << std::endl;
    std::cout << "Num samples per Channels: " << audioData.getNumSamplesPerChannel() << std::endl;
    std::cout << "Length in Seconds: " << audioData.getLengthInSeconds() << std::endl;
    std::cout << std::endl;

    QFile output_file(outFileName);
    if(!output_file.open(QFile::WriteOnly |QFile::Truncate))
    {
        cout << "failed to open output file" << endl;
    }
    QTextStream output(&output_file);

    int channel = 0;
    for (int i = 0; i < audioData.getNumSamplesPerChannel(); i+=vpe.getBufferSize())
    {
        for (int j=0; j<vpe.getBufferSize(); j++)
            vpe.samples[j] = uint16_t(32767 * audioData.samples[0][i+j] + 32767);
        vpe.process();
        cout << "Voice active = " << vpe.voice_active << endl;
        cout << "Frequency = " << vpe.voice_pitch_frequency << " Hz." << endl;

        output << QString::number(i) << ", " << QString::number(vpe.voice_active) << ", " << QString::number(vpe.voice_pitch_frequency) << "\n";
    }
    output_file.close();
}
