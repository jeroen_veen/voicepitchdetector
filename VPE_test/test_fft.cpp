/*! ***************************************************************************
 *
 * \brief     Unit test functions for FFT
 * \file      test_fft.cpp
 * \author    Jeroen Veen
 * \author
 * \date      June 2022
 * \copyright 2020 HAN University of Applied Sciences. All Rights Reserved.
 *            \n\n
 *            Permission is hereby granted, free of charge, to any person
 *            obtaining a copy of this software and associated documentation
 *            files (the "Software"), to deal in the Software without
 *            restriction, including without limitation the rights to use,
 *            copy, modify, merge, publish, distribute, sublicense, and/or sell
 *            copies of the Software, and to permit persons to whom the
 *            Software is furnished to do so, subject to the following
 *            conditions:
 *            \n\n
 *            The above copyright notice and this permission notice shall be
 *            included in all copies or substantial portions of the Software.
 *            \n\n
 *            THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 *            EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 *            OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 *            NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 *            HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 *            WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 *            FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 *            OTHER DEALINGS IN THE SOFTWARE.
 *
 *****************************************************************************/
#include "test_fft.h"
#include <iostream>
#include <stdint.h>
#include "../VPE/fft.h"
#include "unity.h"

using namespace std;

static const uint16_t _fft_size = 32; // must be power of 2
static const uint16_t _sample_rate = 16000; // [Sps]

float src_data_test_case_01[_fft_size] = {0.195090322016128,
                                          0.382683432365090,
                                          0.555570233019602,
                                          0.707106781186548,
                                          0.831469612302545,
                                          0.923879532511287,
                                          0.980785280403230,
                                          1,
                                          0.980785280403230,
                                          0.923879532511287,
                                          0.831469612302545,
                                          0.707106781186548,
                                          0.555570233019602,
                                          0.382683432365090,
                                          0.195090322016129,
                                          0,
                                          -0.195090322016128,
                                          -0.382683432365090,
                                          -0.555570233019602,
                                          -0.707106781186548,
                                          -0.831469612302545,
                                          -0.923879532511287,
                                          -0.980785280403230,
                                          -1,
                                          -0.980785280403230,
                                          -0.923879532511287,
                                          -0.831469612302546,
                                          -0.707106781186548,
                                          -0.555570233019602,
                                          -0.382683432365090,
                                          -0.195090322016129,
                                          0}; // Matlab generated using sin(2*pi*[1:32]/32)

float exp_data_test_case_01[_fft_size] = {0, 0,
                                          3.12144515225805, -15.6925644864517,
                                          0, 0,
                                          0, 0,
                                          0, 0,
                                          0, 0,
                                          0, 0,
                                          0, 0,
                                          0, 0,
                                          0, 0,
                                          0, 0,
                                          0, 0,
                                          0, 0,
                                          0, 0,
                                          0, 0,
                                          0, 0
                                         }; // this is a single sided spectrum, with alternating real and imag nrs

void test_rfft(void)
{
    float fft_input[_fft_size];
    float fft_output[_fft_size];
    FFT fft = FFT(_fft_size, _sample_rate);

    for (int j = 0; j < _fft_size; j++)
        fft_input[j] = src_data_test_case_01[j];
    fft.rfft(fft_input, fft_output, _fft_size);
//    for (int j = 0; j < _fft_size; j++)
//        std::cout << fft_output[j] << std::endl;

    TEST_ASSERT_EQUAL_FLOAT_ARRAY_MESSAGE(exp_data_test_case_01, fft_output, _fft_size, "test case sine wave");
}
