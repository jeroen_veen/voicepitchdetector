QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++17

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0
INCLUDEPATH += $$PWD/unity/src

SOURCES += \
    ../VPE/HampelFilter.cpp \
    ../VPE/biquad.cpp \
    ../VPE/fft.cpp \
    ../VPE/mutex.cpp \
    ../VPE/signal.cpp \
    ../VPE/vpe.cpp \
    ../VPE/zcred.cpp \
    main.cpp \
    mainwindow.cpp \
    test_fft.cpp \
    test_fft.h \
    unity/src/unity.c

HEADERS += \
    ../VPE/HampelFilter.h \
    ../VPE/biquad.h \
    ../VPE/fft.h \
    ../VPE/mutex.h \
    ../VPE/signal.h \
    ../VPE/vpe.h \
    ../VPE/zcred.h \
    AudioFile.h \
    mainwindow.h \
    unity/src/unity.h \
    unity/src/unity_internals.h

FORMS += \
    mainwindow.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

DISTFILES += \
    unity/src/meson.build
