clear all; close all; clc;
range = [0 10];
fs = 16e3;

[x,fileFs] = audioread("..\..\data\Farrakhan.mp3");
x = resample(x,fs,fileFs);
x = x/max(abs(x));

win = hamming(50e-3*fs,"periodic");
[speechIndices,thresholds] = detectSpeech(x,fs,Window=win, MergeDistance=numel(win));

[f0,locs] = pitch(x,fs);
t = (0:length(x)-1)/fs;
t0 = (locs-1)/fs;


figure(1);
tiledlayout(2,1)
nexttile
plot(t, x, 'b')
amax = max(x);
amin = min(x);
for idx = 1:size(speechIndices,1)
    xline(t(speechIndices(idx,1)),'color',[0 0.4470 0.7410],'LineWidth',1.2);
    patch([t(speechIndices(idx,1)),t(speechIndices(idx,1)),t(speechIndices(idx,2)),t(speechIndices(idx,2))], ...
        [amin,amax,amax,amin], ...
        [0.3010 0.7450 0.9330], ...
        'FaceAlpha',0.2,'EdgeColor','none');
    xline(t(speechIndices(idx,2)),'color',[0 0.4470 0.7410],'LineWidth',1.2);
end
ylabel("Amplitude")
title("Pitch Estimation of Clean Signal")
xlim(range)

nexttile
plot(t0, f0, 'r');
amax = max(f0);
amin = min(f0);
for idx = 1:size(speechIndices,1)
    xline(t(speechIndices(idx,1)),'color',[0 0.4470 0.7410],'LineWidth',1.2);
    patch([t(speechIndices(idx,1)),t(speechIndices(idx,1)),t(speechIndices(idx,2)),t(speechIndices(idx,2))], ...
        [amin,amax,amax,amin], ...
        [0.3010 0.7450 0.9330], ...
        'FaceAlpha',0.2,'EdgeColor','none');
    xline(t(speechIndices(idx,2)),'color',[0 0.4470 0.7410],'LineWidth',1.2);
end
ylabel("F0 (Hz)");
xlabel("Time (s)");
xlim(range)