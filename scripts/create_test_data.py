import os
from numpy import arange, pi, sin, round

NR_OF_SAMPLES = 2048
SAMPLE_RATE = 16000
f_test = 100
name = "test_data"

signal = round(2**13*(1+sin(2*pi*f_test*arange(0,NR_OF_SAMPLES)/SAMPLE_RATE))).astype(int)

filename = os.path.sep.join([os.getcwd(), "src", name + ".h"])

print(filename)
file = open(filename, "w")
file.writelines([
    f"#ifndef __{name.upper()}__\n",
    f"#define __{name.upper()}__\n\n",
    f"int16_t {name}[{NR_OF_SAMPLES}] = {{",
    # "\tconst float T_s = {}; // input sample rate [s]\n".format(T_s_1),
    # "\tconst int M = {}; // rate reduction factor\n".format(M),
    # "\tconst int K = {}; // impulse response length\n".format(L1),
    # "\tconst float h[{}] = {{".format(L1),
])
for i, c in enumerate(signal):
    if i % 20 == 0:
        file.write("\n\t")
    if i < len(signal)-1:
        file.write("{:5d},".format(c))
    else:
        file.write("{:5d}}}; // test signal\n".format(c))

file.writelines([
    # "\t\t};\n",
    # "};\n",
    "\n",
    f"#endif // {name.upper()}\n"
])

file.close()