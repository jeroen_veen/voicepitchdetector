import matplotlib.transforms as mtransforms
import numpy as np
from scipy import signal
import matplotlib.pyplot as plt
from group_delay import group_delay
import os

##filename = "20210527_0927_Rudie_voor_medicatie"
save_post_fix = None #'_loco_1Hz'

f_s = 16000 # [Hz] sampling rate
f_voice_pitch = [40, 400] # [Hz] speech fundamental frequency band

# doit
T_s = 1/f_s
N = 4096 # frequency points
f = np.arange(0, f_s/2, f_s/(2*N))

# Design front-end filters
sos_voice_pitch = signal.butter(N=2, Wn=f_voice_pitch, btype='bandpass', output='sos', fs=f_s)
scale = sos_voice_pitch[0,0]
sos_voice_pitch[0,0:3] /= scale
print(f'voice pitch front-end filter design:\n\t{sos_voice_pitch} \n\t with scale:{scale}')
_, H_voice_pitch = signal.sosfreqz(sos_voice_pitch, worN=N, fs=f_s)  # Calculate the frequency response
H_voice_pitch *= scale
gd_H_voice_pitch = group_delay(H_voice_pitch, f)
delay = gd_H_voice_pitch[np.argmin(np.abs(f-np.mean(f_voice_pitch)))] # group delay in passband
print('voice pitch front-end filter group delay estimate = {} s'.format(delay))


# Plot results
fig0, ax0 = plt.subplots()
ax0.grid(True)
ax0.set_title("Front-end filter")
ax0.set_ylim(-40, 6)
ax0.set_xlabel('f[Hz]')
ax0.set_ylabel('Magnitude [dB]')

ax0.semilogx(f, 20 * np.log10(abs(H_voice_pitch + 1e-15)), 'b--')
ax0.axhline(-3, color='black', linestyle=':')
ax0.axvline(f_s/2, color='black', linestyle='--', linewidth=2, alpha=0.1)
trans = mtransforms.blended_transform_factory(ax0.transData, ax0.transAxes)
ax0.fill_between(f, 0, 1, where=(f > f_voice_pitch[0]) & (f < f_voice_pitch[1]),
                 facecolor='green', alpha=0.1, transform=trans)

fig1, ax1 = plt.subplots()
ax1.grid(True)
ax1.set_ylim(0, 0.015)
ax1.set_title("Front-end filter group delay")
ax1.set_xlabel('f[Hz]')
ax1.set_ylabel('Group delay [s]')

ax1.semilogx(f, gd_H_voice_pitch, 'b--')

trans = mtransforms.blended_transform_factory(ax0.transData, ax0.transAxes)
ax1.fill_between(f, 0, 1, where=(f > f_voice_pitch[0]) & (f < f_voice_pitch[1]),
                 facecolor='green', alpha=0.1, transform=trans)

plt.show(block=True)
