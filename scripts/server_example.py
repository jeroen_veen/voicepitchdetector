"""
Very simple HTTP server in python for logging data
"""
from http.server import BaseHTTPRequestHandler, HTTPServer
from os import remove

host =  "192.168.1.15" # ''  # "localhost" 
port = 5003
filename = "test.raw"


class MyServer(BaseHTTPRequestHandler):

    def _set_response(self):
        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.end_headers()

    def do_GET(self):
        self.send_response(200)
        self.send_header("Content-type", "text/html")
        self.end_headers()
        self.wfile.write(bytes("<html><head><title>Test</title></head>", "utf-8"))
        self.wfile.write(bytes("<p>Request: %s</p>" % self.path, "utf-8"))
        self.wfile.write(bytes("<body>", "utf-8"))
        self.wfile.write(bytes("<p>This is an example web server.</p>", "utf-8"))
        self.wfile.write(bytes("</body></html>", "utf-8"))

    def do_POST(self):
        print("post received")
        content_length = int(self.headers['Content-Length'])  # <--- Gets the size of data
        post_data = self.rfile.read(content_length)  # <--- Gets the data itself
        self._set_response()

        with open(filename, "ab") as file:
            file.write(post_data)


if __name__ == '__main__':
    try:
        remove(filename)
    except:
        pass
    webServer = HTTPServer((host, port), MyServer)
    print(f"Server started http://{host}:{port}")
    try:
        webServer.serve_forever()
    except KeyboardInterrupt:
        pass
    webServer.server_close()
    print("Server stopped.")
