import numpy as np

def group_delay(H, f):
    phi = np.angle(H)
    phi = np.unwrap(2*phi)/2  # Remove discontinuities
    df_phi = np.diff(phi)
    tau_g = -df_phi/(2*np.pi*np.diff(f))
    tau_g = np.insert(tau_g, 0, 0)
    return tau_g