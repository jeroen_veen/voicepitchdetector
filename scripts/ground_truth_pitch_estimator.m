clear all; close all; clc;
range = [0 10];

file_name_pattern = "C:\Users\scbry\OneDrive - HAN\APPS\Jeroen Veen\data\RDH_VL\*.wav"
fs = 16000;
%
file_list = dir(file_name_pattern);
%
for i = 1:1 %length(file_list)
    file = file_list(i);
    disp(file.name);

    [x,fileFs] = audioread(append(file.folder, '\', file.name));
    win = hamming(50e-3*fileFs,"periodic");
    [P,Q] = rat(fs/fileFs);
    x = resample(x,P,Q);
    x = x/max(abs(x));
    audiowrite(append(file.folder, '\', string(fs), '_', file.name), x, fs);
    [speechIndices,thresholds] = detectSpeech(x,fs,Window=win, MergeDistance=numel(win));
    [f0,locs] = pitch(x,fs);
    data_frame = struct('file_name', file.name, ...
        'samplerate', fileFs, ...
        'speechIndices', speechIndices, ...
        'thresholds', thresholds, ...
        'f0', f0, 'pitchIndices', locs);
    txt = jsonencode(data_frame);
    fid = fopen(append(file.folder, '\', file.name, '.txt'),'w+');
    fprintf(fid, '%s\n', txt);
    fclose(fid);
end

% dump to csv
fid = fopen(append(file.folder, '\', file.name, '.csv'),'w+');

for i=1:length(locs)
    fprintf(fid, '%d, %f\n', locs(i), f0(i));
end
fclose(fid);

% 
% 
% [x,fileFs] = audioread("..\..\data\Farrakhan.mp3");
% x = resample(x,fs,fileFs);
% x = x/max(abs(x));
% 
% win = hamming(50e-3*fs,"periodic");
% [speechIndices,thresholds] = detectSpeech(x,fs,Window=win, MergeDistance=numel(win));
% 
t = (0:length(x)-1)/fs;
t0 = (locs-1)/fs;


figure(1);
tiledlayout(2,1)
nexttile
plot(t, x, 'b')
amax = max(x);
amin = min(x);
for idx = 1:size(speechIndices,1)
    xline(t(speechIndices(idx,1)),'color',[0 0.4470 0.7410],'LineWidth',1.2);
    patch([t(speechIndices(idx,1)),t(speechIndices(idx,1)),t(speechIndices(idx,2)),t(speechIndices(idx,2))], ...
        [amin,amax,amax,amin], ...
        [0.3010 0.7450 0.9330], ...
        'FaceAlpha',0.2,'EdgeColor','none');
    xline(t(speechIndices(idx,2)),'color',[0 0.4470 0.7410],'LineWidth',1.2);
end
ylabel("Amplitude")
title("Pitch Estimation of Clean Signal")
xlim(range)

nexttile
plot(t0, f0, 'r');
amax = max(f0);
amin = min(f0);
for idx = 1:size(speechIndices,1)
    xline(t(speechIndices(idx,1)),'color',[0 0.4470 0.7410],'LineWidth',1.2);
    patch([t(speechIndices(idx,1)),t(speechIndices(idx,1)),t(speechIndices(idx,2)),t(speechIndices(idx,2))], ...
        [amin,amax,amax,amin], ...
        [0.3010 0.7450 0.9330], ...
        'FaceAlpha',0.2,'EdgeColor','none');
    xline(t(speechIndices(idx,2)),'color',[0 0.4470 0.7410],'LineWidth',1.2);
end
ylabel("F0 (Hz)");
xlabel("Time (s)");
xlim(range)