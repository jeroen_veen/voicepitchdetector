#pragma once
#define USE_SPLIT_RADIX 1
#define LARGE_BASE_CASE 1
#ifndef TWO_PI
#define TWO_PI 6.28318530
#endif

class FFT
{
public:
    FFT(int size, int samplefreq);
    ~FFT();
    void rfft(float *x, float *y, int n);

private:
    int _size; // FFT size
    float *_twiddle_factors; // pointer to buffer holding twiddle factors

private:
    void split_radix_fft(float *x, float *y, int n, int stride, int tw_stride);
    void fft8(float *input, int stride_in, float *output, int stride_out);
    void fft4(float *input, int stride_in, float *output, int stride_out);
};
