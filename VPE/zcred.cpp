#include "zcred.h"
// Zero crossing rising edge detector
//  detect rising edges at zerocrossings,
//  return sample count since previous rising edge

uint16_t Zcred::process(const int16_t in)
{
    uint16_t ret = 0; // return variable

    switch (state)
    {
    case 0: // initialize
        count = 0;
        if (in == 0)
        { // unexpected
        }
        else if (in < 0)
        { // start looking for rising edge
            state = 1;
        }
        else
        { // start looking for falling edge
            state = 2;
        }
        break;
    case 1: // looking for rising edge
        count++;
        if (in > 0)
        {                // zero crossing detected
            state = 2;   // start looking for falling edge
            ret = count; // return the sample count since previous rising edge
            count = 0;   // reset sample counter
        }
        break;
    case 2: // looking for falling edge
        count++;
        if (in < 0)
        {              // zero crossing detected
            state = 1; // start looking for rising edge
        }
        break;
    default:
        break;
    }
    return ret;
}

void Zcred::reset()
{
    state = 0;
    count = 0;
}
