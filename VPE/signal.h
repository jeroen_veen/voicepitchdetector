
int compute_Hamming_window_function(float *w, int n);
void clear_array(float *x, int n);
void remove_DC(float *x, int n);
void apply_window(float *x, float *w, int n);