#pragma once
#include <stdint.h>
#include "fft.h"
#include "biquad.h"
#include "zcred.h"
#include "HampelFilter.h"
#include <math.h>

class VPE
{
private:
    // hidden parameters
    static const uint16_t _sample_buffer_size = 4096; //4096; // 8192; // 16384; // must be power of 2
    static const uint16_t _fft_size = 2048;           // must be power of 2
    static const uint16_t _sample_buffer_step = _fft_size >> 1;
    static const uint16_t _sample_rate = 16000; // [Sps]
    // const float f_c_low = 40, f_c_high = 1000;
    // const float b_sos_1[3] = {1.0,  2.0, 1.0}, a_sos_1[3] = {1.0, -1.4947403, 0.60661452}; // 40-1000Hz front end filter, section 1
    // const float b_sos_2[3] = {1.0, -2.0, 1.0}, a_sos_2[3] = {1.0, -1.97249003, 0.97290715}; // 40-1000Hz front end filter, section 2
    // const float sos_scale = 0.02734515201919621;
    // const float f_c_low = 40, f_c_high = 500;
    // const float b_sos_1[3] = {1.0, 2.0, 1.0}, a_sos_1[3] = {1.0, -1.77048436, 0.79975634};  // 40-500Hz front end filter, section 1
    // const float b_sos_2[3] = {1.0, -2.0, 1.0}, a_sos_2[3] = {1.0, -1.97343892, 0.97388909}; // 40-500Hz front end filter, section 2
    // const float sos_scale = 0.006925793172452106;
    const float f_c_low = 50, f_c_high = 400;
    const float b_sos_1[3] = {1.0, 2.0, 1.0}, a_sos_1[3] = {1.0, -1.81717983, 0.83630344};  // 40-400Hz front end filter, section 1
    const float b_sos_2[3] = {1.0, -2.0, 1.0}, a_sos_2[3] = {1.0, -1.97876951, 0.97905842}; // 40-400Hz front end filter, section 2
    const float sos_scale = 0.00453621771580351;
    const float alpha = 0.10; // smoothing factor

public:
    uint8_t _bclkPin, _lrckPin, _dinPin;
    float zero_crossing_rate;
    FFT fft = FFT(_fft_size, _sample_rate);
    float fft_input[_fft_size];
    float fft_output[_fft_size];
    float window_function[_fft_size];
    float bin_width; // [Hz]
    float periodogram[_fft_size >> 1];
    float cepstrum[_fft_size >> 1];
    int16_t samples[_sample_buffer_size];
    Biquad *sos_1 = new Biquad(b_sos_1, a_sos_1, sos_scale);
    Biquad *sos_2 = new Biquad(b_sos_2, a_sos_2, 1.0);
    Zcred *zcred = new Zcred(); // zero-crossing detector
    HampelFilter *hampel = new HampelFilter(100.0, 11, 3.50);
    float avg_power = 0, min_power = 1000, max_power = 0;
    float delta_f = float(_sample_rate)/float(_fft_size);
    int16_t i_min = round(f_c_low/delta_f); // minimum frequency domain index to consider
    int16_t i_max = round(f_c_high/delta_f); // maximum frequency domain index to consider
#ifdef ESP_PLATFORM
    SemaphoreHandle_t mutex;
#else
    int mutex;
#endif

public:
    static void i2sSamplerTask(void *param);
    bool preprocess();
    bool compute_periodogram();
    bool detect_voice_activity();

public:
    VPE();
    bool process();
    bool getResult(float *freq, float *mag);
    int getBufferSize() { return _sample_buffer_size;};
    bool voice_active = false;
    float voice_pitch_frequency = 0;
    float voice_pitch_magnitude = 0;
};
