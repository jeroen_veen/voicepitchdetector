#include <math.h>
#include "vpe.h"
#include "signal.h"
#include "mutex.h"

VPE::VPE()
{
#ifdef ESP_PLATFORM
    // Create mutex
    mutex = xSemaphoreCreateMutex();
    assert(mutex);
#endif

    // Set-up variables
    compute_Hamming_window_function(window_function, _fft_size);
    clear_array(periodogram, _fft_size >> 1);
    clear_array(cepstrum, _fft_size >> 1);

    // compute power of window in time and frequency domain
    double total_power_time_domain = 0;
    for (int j = 0; j < _fft_size; j++)
    {
        total_power_time_domain += window_function[j] * window_function[j]; // / _sample_rate;
    }
    total_power_time_domain /= _fft_size;
//    log_i("Window power (time domain): %f", 10 * log10(total_power_time_domain));

    fft.rfft(window_function, fft_output, _fft_size);
    double total_power_freq_domain = 0;
    for (int j = 0; j < (_fft_size >> 1); j++)
    {
        float factor = (j == 0) ? 1 : 2; // make sure to collect power at both sides of the spectrum
        float power = factor * (fft_output[2 * j] * fft_output[2 * j] + fft_output[2 * j + 1] * fft_output[2 * j + 1]) / float(_fft_size);
        total_power_freq_domain += power; // / _sample_rate;
    }
    total_power_freq_domain /= _fft_size;

//    log_i("Window power (frequency domain): %f", 10 * log10(total_power_freq_domain));
}

bool VPE::getResult(float *freq, float *mag)
{
//    take_mutex(mutex);
    *freq = voice_pitch_frequency;
    *mag = voice_pitch_magnitude;
    bool ret = voice_active;
//    give_mutex(mutex);

    return ret;
}

bool VPE::preprocess()
{
    // pre-processing: bandpass filtering and time-domain feature computation
    const float alpha = 0.5;
    int last_event_i = 0;

    zero_crossing_rate = 0; // reset
    for (int i = 0; i < _sample_buffer_size; i++)
    {
        // front-end filter: 2 second order sections
        samples[i] = sos_2->process(sos_1->process(samples[i]));
        // zero crossing detector
        if (zcred->process(samples[i]) > 0)
        {
            // log_i("zcd: %d", i - last_event_i);
            if (i > last_event_i)
            {
                float temp_zcr = (float)(_sample_rate) / (float)(i - last_event_i); // [Hz]
                zero_crossing_rate = alpha * zero_crossing_rate + (1 - alpha) * temp_zcr;
            }
            last_event_i = i;
            // zero_crossing_rate = hampel->checkIfOutlier(temp_zcr) ? hampel->readMedian() : temp_zcr;
            // hampel->write(temp_zcr);
        }
    }
    return true;
}

bool VPE::compute_periodogram()
{
    constexpr float eps = std::numeric_limits<float>::epsilon();
    float max_val = 0, freq = 0;

    //  overlapped time windows so nonstationary changes can be measured correctly
    for (int i = 0; i < _sample_buffer_size - _sample_buffer_step; i += _sample_buffer_step)
    {
        double power = 0;
        for (int i = 0; i < _sample_buffer_size; i++)
            power += (double)(samples[i] * samples[i]); // hope we don't clip
        power /= (double)(_sample_buffer_size);         // average power
        avg_power = alpha * avg_power + (1 - alpha) * (float)power;

        for (int j = 0; j < _fft_size; j++)
            fft_input[j] = ((float)samples[i + j]) / 32768.0;
        remove_DC(fft_input, _fft_size);
        apply_window(fft_input, window_function, _fft_size);
        fft.rfft(fft_input, fft_output, _fft_size);

        // Estimate single-sided periodogram
        for (int j = 0; j < (_fft_size >> 1); j++)
        {
            float factor = (j == 0) ? 1 : 2; // make sure to collect power at both sides of the spectrum
            float inst_power = factor * (fft_output[2 * j] * fft_output[2 * j] + fft_output[2 * j + 1] * fft_output[2 * j + 1]) / float(_fft_size);
            periodogram[j] = alpha * periodogram[j] + (1 - alpha) * inst_power; // * FFT_SIZE / SAMPLE_RATE;

            // reload fft for cepstrum analysis
            fft_input[j] = log(periodogram[j]) + eps;
//            fft_input[2*j+1] = 0; //
            fft_input[_fft_size-j-1] = fft_input[j];  // reconstruct double sided spectrum
        }

        // Estimate single-sided cepstrum
        fft.rfft(fft_input, fft_output, _fft_size);
        for (int j = 0; j < (_fft_size >> 1); j++)
        {
            float factor = (j == 0) ? 1 : 2; // make sure to collect power at both sides of the spectrum
            float inst_cepstrum = factor * (fft_output[2 * j] * fft_output[2 * j] + fft_output[2 * j + 1] * fft_output[2 * j + 1]) / float(_fft_size);
            cepstrum[j] = alpha * cepstrum[j] + (1 - alpha) * inst_cepstrum; // * FFT_SIZE / SAMPLE_RATE;
        }

        // Find peak within a range. Note that the resolution could be improved using a parabola fitting trick
        max_val = 0, freq = 0;
        for (int j = i_min; j < i_max; j++)
        {
            if (periodogram[j] > max_val)
            {
                max_val = periodogram[j]; // / bin_width);
                freq = float(j) * delta_f;
            }
        }
//        take_mutex(mutex);
        voice_pitch_frequency = alpha * voice_pitch_frequency + (1 - alpha) * freq;
        voice_pitch_magnitude = alpha * voice_pitch_magnitude + (1 - alpha) * max_val;
//        give_mutex(mutex);
    }
    return true;
}

bool VPE::detect_voice_activity()
{
    const float thres_alpha = .9999;
    // todo: should the features be filtered by alow-pass filter or a hampel?

    // estimate signal extremes by fast-attack, slow-decay filtering
    if (avg_power < min_power)
        min_power = avg_power;
    else
        min_power = thres_alpha * min_power + (1 - thres_alpha) * avg_power;
    if (avg_power > max_power)
        max_power = avg_power;
    else
        max_power = thres_alpha * max_power + (1 - thres_alpha) * avg_power;

    // dynamic power thresholding
    float log_avg_power = 10 * log10(avg_power);
    float log_min_power = 10 * log10(min_power);
    float log_max_power = 10 * log10(max_power);
    float thres_lambda = (log_max_power - log_min_power) / log_max_power;
    float power_threshold = (1.0 - thres_lambda) * log_max_power + thres_lambda * log_min_power;
    int feature_0 = log_avg_power > power_threshold;

    // frequency windowing
    int feature_1 = (voice_pitch_frequency > f_c_low) && (voice_pitch_frequency < f_c_high);

    // compute spectral features
    float num = 0, den = 0;
    for (int j = 0; j < _fft_size; j++)
    {
        float f = (float)j * bin_width;
        if ((f > f_c_low) && (f < f_c_high))
        {
            num += f * periodogram[j];
            den += periodogram[j];
        }
    }
    float spectral_centroid = num / den;
    float num1 = 0, num2 = 0;
    for (int j = 0; j < _fft_size; j++)
    {
        float f = (float)j * bin_width;
        if ((f > f_c_low) && (f < f_c_high))
        {
            float f_n = f - spectral_centroid;
            num1 += pow(f_n, 2) * periodogram[j];
            num2 += pow(f_n, 3) * periodogram[j];
        }
    }
    float spectral_spread_squared = num1 / den;
    float spectral_skewness = num2 / (pow(spectral_centroid, 3) * den);

    voice_active = (feature_0 && feature_1) ? true : false;

    if (feature_0 && feature_1)
    {
        // log_i("min, max, avg, thres power: %f, %f, %f, %f [dB]: %i", log_min_power, log_max_power, log_avg_power, power_threshold, log_avg_power > power_threshold);
//        log_i("power density peak: %f [dB/Hz] @ %f [Hz] zero crossing rate: %f [Hz] ", voice_pitch_magnitude, voice_pitch_frequency, zero_crossing_rate);
//        take_mutex(mutex);
        voice_active = true;
//        give_mutex(mutex);
    }
    return true;
}

bool VPE::process()
{
    preprocess();
    compute_periodogram();
    detect_voice_activity();

    return 0;
}
