#include "signal.h"
#include "math.h"

#ifndef TWO_PI
#define TWO_PI 6.28318530
#endif

int compute_Hamming_window_function(float *w, int n)
{
    if ((n & (n - 1)) != 0) // tests if size is a power of two
        return -1;
    for (int i = 0; i < (n >> 1); i++)
    {
        float ratio = float(i) / float(n);
        float weighingFactor = 0.54 - (0.46 * cos(TWO_PI * ratio));
        w[i] = weighingFactor;
        w[n - (i + 1)] = weighingFactor;
    }
    return 0;
}

void clear_array(float *x, int n)
{
    for (int i = 0; i < n; i++)
        x[i] = 0;
}

void remove_DC(float *x, int n)
{
    double avg = 0;
    for (int i = 0; i < n; i++)
        avg += x[i];
    avg /= n;
    for (int i = 0; i < n; i++)
        x[i] -= float(avg);
}

void apply_window(float *x, float *w, int n)
{
    for (int i = 0; i < n; i++)
        x[i] *= w[i];
}