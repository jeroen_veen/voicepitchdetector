#pragma once
#include <stdint.h>

class Zcred
{
private:
    uint8_t state = 0;  // detector state
    uint16_t count = 0; // sample counter

public:
    Zcred() { reset(); };
    ~Zcred(){};
    uint16_t process(const int16_t in);
    void reset();
};
