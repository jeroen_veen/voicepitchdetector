#include "biquad.h"

// direct form 2 implementation:
//      w[0] = in - a[1] * w[1] - a[2] * w[2];
//      float out = b[0] * w[0] + b[1] * w[1] + b[2] * w[2];
//      w[2] = w[1];
//      w[1] = w[0]; // shift samples
//
// init:
//      w = w[0] = w[1] = w[2] = in - a[1] * w - a[2] * w = in - w * (a[1] + a[2]) => w = in / (1 + a[1] + a[2])
//      out = w * (b[0] + b[1] + b[2]) = in * (b[0] + b[1] + b[2]) / (1 + a[1] + a[2])
//
//
// transposed direct form 2 implementation:
// preferred since zeroes are implemented first, thus reducing explosion of internal states
//      out = b[0] * in + w[0];
//      w[0] = w[1] + b[1] * in - a[1] * out;
//      w[1] = b[2] * in - a[2] * out;
//
// init:
//      w[0] = w[1] + b[1] * in - a[1] * out = b[2] * in - a[2] * out + b[1] * in - a[1] * out = (b[1] + b[2]) * in - (a[1] + a[2]) * out
//           = (b[1] + b[2]) * in - (a[1] + a[2]) * (b[0] * in + w[0]) = (b[1] + b[2] - b[0] * (a[1] + a[2])) * in - (a[1] + a[2]) * w[0]
//      w[0] * (1 + a[1] + a[2]) = (b[1] + b[2] - b[0] * (a[1] + a[2])) * in
//      w[0] = in * (b[1] + b[2] - b[0] * (a[1] + a[2])) / (1 + a[1] + a[2])
//      out = b[0] * in + w[0];

Biquad::Biquad(const float set_b[3], const float set_a[3])
{
    for (int i = 0; i < 3; i++)
    { // copy coefficients
        b[i] = set_b[i];
        a[i] = set_a[i];
    }
}

Biquad::Biquad(const float set_b[3], const float set_a[3], const float scale)
{
    scaleValue = scale;
    for (int i = 0; i < 3; i++)
    { // copy coefficients
        b[i] = set_b[i];
        a[i] = set_a[i];
    }
}

float Biquad::process(const float in)
// transposed direct form II implementation
// preferred since zeroes are implemented first, thus reducing explosion of internal states
{
    float out = b[0] * in + w[0];
    w[0] = w[1] + b[1] * in - a[1] * out;
    w[1] = b[2] * in - a[2] * out;
    out *= scaleValue;

    return out;
}

void Biquad::reset()
{
    w[0] = w[1] = 0;
}

float Biquad::init(const float in)
// initialize the filter memory given a steady-state input and return output
{
    w[0] = in * (b[1] + b[2] - b[0] * (a[1] + a[2])) / (1 + a[1] + a[2]);
    float out = b[0] * in + w[0];
    out *= scaleValue;
    w[1] = b[2] * in - a[2] * out;

    return out;
}
