#pragma once

class Biquad
{
private:
  float scaleValue = 1;
  float a[3], b[3], w[2] = {0};

public:
  Biquad(){};
  Biquad(const float *b, const float *a);
  Biquad(const float set_b[3], const float set_a[3], const float scale);
  ~Biquad(){};
  float process(const float in);
  float init(const float in);
  void reset();
};

