#include "webSocket.h"
#include <HTTPClient.h>
#include "WiFiCredentials.h"
#include "queue_data.h"

// parameters
char *_server_url;
QueueHandle_t _queue = NULL;

void sendData(WiFiClient *wifiClient, HTTPClient *httpClient, const char *url, uint8_t *bytes, size_t count)
{
    // send data to the server
    httpClient->begin(*wifiClient, url);
    // log_d("Sending %d bytes from %s:%d to %s:%d", count, wifiClient->localIP().toString().c_str(), wifiClient->localPort(), wifiClient->remoteIP().toString().c_str(), wifiClient->remotePort());
    httpClient->addHeader("content-type", "application/octet-stream");
    httpClient->POST(bytes, count);
    httpClient->end();
}

void webSocketTask(void *param)
{
    // setup the HTTP Client
    WiFiClient *wifiClient = new WiFiClient();
    HTTPClient *httpClient = new HTTPClient();
    const TickType_t xDelay = 10 / portTICK_PERIOD_MS;
    data d;

    while (true)
    {
        vTaskDelay(xDelay); // make sure other tasks get some time too
        if (xQueueReceive(_queue, &d, (TickType_t)xDelay))
        {
            sendData(wifiClient, httpClient, _server_url, (uint8_t *)d._buffer, d._size * sizeof(uint16_t));
        }
    }
}

esp_err_t startWSTask(const char *server_url, QueueHandle_t dataQueue)
{
    _server_url = (char *)server_url;

    // set up the task
    _queue = dataQueue;
    xTaskCreatePinnedToCore(webSocketTask, "WebSocket Task", 4096, NULL, 1, NULL, 1);

    return ESP_OK;
}
