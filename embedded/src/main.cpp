/*
Speaking voice pitch estimation
    v0.0, 220208, Jeroen Veen, first release
    v0.1, 

Simply computing a spectrogram using Welch method and selecting the peak amplitude.
Tested on a tonefork, but much improvement needed to get this to work for human voice.

For debugging, you can stream the audio via websocket over Wifi.
Set your wifi credentials in a WiFiCredentials.h file, defining SSID and PASSWORD.
Use a server, like server_example.py to dump raw data to a file.
With an audio editing software, like Audacity, import a file use the "raw" import option on Audacity -
 this will let you specify the size of the samples (signed 16 bit), number of channels (1 channel),
 and sample rate (16KHz is the default in the code).

Additional info:
    I2s MEMS microphone = SPH0645LM4H
    platform = espressif32
    board = tinypico
    framework = arduino
    monitor_speed = 115200
    build_flags = -DCORE_DEBUG_LEVEL=5
    lib_deps =
        tinypico/TinyPICO Helper Library@^1.4.0
*/

#define CONFIG_ARDUHAL_ESP_LOG // used to send logging data to serial monitor
#include <TinyPICO.h>
#include "pitchEstimator.h"
#include <WiFi.h>     // Wifi library
#include <esp_wpa2.h> //wpa2 library for connections to Enterprise networks
#include "WiFiCredentials.h"

// Define your ESP32 I2S wiring
const uint8_t _bclkPin = 27; // I2S Clock
const uint8_t _lrckPin = 25; // Left/Right audio
const uint8_t _dinPin = 26;  // I2S Data
// char _i2s_data_server_url[] = "http://192.168.1.15:5003/i2s_samples";
char _i2s_data_server_url[] = "http://145.74.217.32/i2s_samples";

TinyPICO tp = TinyPICO();
Pitch vcp = Pitch(_bclkPin, _lrckPin, _dinPin);

void connect_wifi()
{
    log_i("Connecting to WiFi %s", SSID);
    WiFi.disconnect(true); // disconnect from wifi to set new wifi connection
    WiFi.mode(WIFI_STA);   // init wifi mode

    if (strcmp(SSID, "eduroam") == 0)
    {
        // esp_wifi_sta_wpa2_ent_set_ca_cert((uint8_t *)test_root_ca, strlen(test_root_ca) + 1);
        esp_wifi_sta_wpa2_ent_set_ca_cert((uint8_t *)test_root_ca, strlen(test_root_ca));
        esp_wifi_sta_wpa2_ent_set_identity((uint8_t *)EAP_IDENTITY, strlen(EAP_IDENTITY));
        esp_wifi_sta_wpa2_ent_set_username((uint8_t *)EAP_IDENTITY, strlen(EAP_IDENTITY));
        esp_wifi_sta_wpa2_ent_set_password((uint8_t *)PASSWORD, strlen(PASSWORD));
        // WiFi.enableSTA(true);
        esp_wifi_sta_wpa2_ent_enable();                 // set config settings to enable function
        WiFi.begin(SSID);                                      // connect to wifi
    }
    else
    {
        WiFi.begin(SSID, PASSWORD); // connect to wifi
    }
    WiFi.setHostname("RandomHostname"); // set Hostname for your device - not neccesary
    while (WiFi.status() != WL_CONNECTED)
    {
        delay(500);
        Serial.print(".");
    }
    log_i("WiFi is connected! IP address set: {%s}", WiFi.localIP());
}

void setup()
{
    const int LED_BUILTIN = 2;
    pinMode(LED_BUILTIN, OUTPUT);
    digitalWrite(LED_BUILTIN, HIGH);

    Serial.begin(115200);
    log_d("%s", __func__);

    uint64_t chipid = ESP.getEfuseMac(); // chip ID equals MAC address(length: 6 bytes).
    log_d("ESP32 rev: %d,  ID = %04X", ESP.getChipRevision(),
          (uint16_t)(chipid >> 32)); // print High 4 bytes
    log_d("%08X", (uint32_t)chipid); // print Low 4bytes.
    log_d("\tFlash size: %u kB, speed: %u, mode: %u",
          ESP.getFlashChipSize() >> 10, ESP.getFlashChipSpeed(), ESP.getFlashChipMode());
    log_d("\tSketch size: %u kB,  %u kB free", ESP.getSketchSize() >> 10, ESP.getFreeSketchSpace() >> 10);
    log_d("\tTotal heap size: %u kB,  %u kB free", ESP.getHeapSize(), ESP.getFreeHeap());
    log_d("\tTotal PSRAM: %d kB,  %u kB free", ESP.getPsramSize(), ESP.getFreePsram());

    vcp.begin();
    // If you want to stream I2S sensor data via websocket, then connect to wifi and supply a server url
    // connect_wifi();
    // vcp.begin(_i2s_data_server_url);

    digitalWrite(LED_BUILTIN, LOW);
    log_i("Set-up done");
}

void loop()
{
    float frequency, magnitude;

    if (vcp.getResult(&frequency, &magnitude))
        log_i("power density peak: %f [dB/Hz] @ %f [Hz]", magnitude, frequency);

    // Do nothing but allow yielding to lower-priority tasks
    vTaskDelay(1000 / portTICK_PERIOD_MS);
}
