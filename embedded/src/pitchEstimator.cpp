#include "pitchEstimator.h"
#include "webSocket.h"
#include <freertos/FreeRTOS.h>
#include "I2SMEMSSampler.h"
#include "mutex.h"
#include "signal.h"
/*
Speaking voice pitch estimator for ESP32

Copyright 2022 Jeroen Veen

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

Pitch::Pitch(uint8_t bclkPin, uint8_t lrckPin, uint8_t dinPin)
{
    _bclkPin = bclkPin;
    _lrckPin = lrckPin;
    _dinPin = dinPin;

    // Create mutex
    mutex = xSemaphoreCreateMutex();
    assert(mutex);

    // Set-up variables
    compute_Hamming_window_function(window_function, _fft_size);
    clear_array(periodogram, _fft_size >> 1);

    // compute power of window in time and frequency domain
    double total_power_time_domain = 0;
    for (int j = 0; j < _fft_size; j++)
    {
        total_power_time_domain += window_function[j] * window_function[j]; // / _sample_rate;
    }
    total_power_time_domain /= _fft_size;
    log_i("Window power (time domain): %f", 10 * log10(total_power_time_domain));

    fft.rfft(window_function, fft_output, _fft_size);
    double total_power_freq_domain = 0;
    for (int j = 0; j < (_fft_size >> 1); j++)
    {
        float factor = (j == 0) ? 1 : 2; // make sure to collect power at both sides of the spectrum
        float power = factor * (fft_output[2 * j] * fft_output[2 * j] + fft_output[2 * j + 1] * fft_output[2 * j + 1]) / float(_fft_size);
        total_power_freq_domain += power; // / _sample_rate;
    }
    total_power_freq_domain /= _fft_size;
    log_i("Window power (frequency domain): %f", 10 * log10(total_power_freq_domain));
}

esp_err_t Pitch::begin(char *web_socket_url)
{
    // set up the tasks
    xTaskCreatePinnedToCore(i2sSamplerTask, "I2S Sampler Task", 2048, this, 1, NULL, 1);

    // launch WiFi
    // TODO: make data streaming a task encapsulated in an object too.
    if (strlen(web_socket_url) > 0)
        startWSTask(web_socket_url, dataQueue);

    return ESP_OK;
}

bool Pitch::getResult(float *freq, float *mag)
{
    take_mutex(mutex);
    *freq = voice_pitch_frequency;
    *mag = voice_pitch_magnitude;
    bool ret = voice_active;
    give_mutex(mutex);

    return ret;
}

bool Pitch::preprocess()
{
    // pre-processing: bandpass filtering and time-domain feature computation
    const float alpha = 0.5;
    int last_event_i = 0;

    zero_crossing_rate = 0; // reset
    for (int i = 0; i < _sample_buffer_size; i++)
    {
        // front-end filter: 2 second order sections
        samples[i] = sos_2->process(sos_1->process(samples[i]));
        // zero crossing detector
        if (zcred->process(samples[i]) > 0)
        {
            // log_i("zcd: %d", i - last_event_i);
            if (i > last_event_i)
            {
                float temp_zcr = (float)(_sample_rate) / (float)(i - last_event_i); // [Hz]
                zero_crossing_rate = alpha * zero_crossing_rate + (1 - alpha) * temp_zcr;
            }
            last_event_i = i;
            // zero_crossing_rate = hampel->checkIfOutlier(temp_zcr) ? hampel->readMedian() : temp_zcr;
            // hampel->write(temp_zcr);
        }
    }
    return true;
}

bool Pitch::compute_periodogram()
{
    // periodogram computation and frequency-domain feature computation
    const float alpha = 0.5;
    float max_power_density = 0, freq = 0;
    float bin_width = float(_sample_rate) / float(_fft_size);

    //  overlapped time windows so nonstationary changes can be measured correctly
    for (int i = 0; i < _sample_buffer_size - _sample_buffer_step; i += _sample_buffer_step)
    {

        double power = 0;
        for (int i = 0; i < _sample_buffer_size; i++)
            power += (double)(samples[i] * samples[i]); // hope we don't clip
        power /= (double)(_sample_buffer_size);         // average power
        avg_power = alpha * avg_power + (1 - alpha) * (float)power;

        for (int j = 0; j < _fft_size; j++)
#ifdef TEST
            fft_input[j] = (float)test_data[j] / 32768.0;
#else
            fft_input[j] = ((float)samples[i + j]) / 32768.0;
#endif
        remove_DC(fft_input, _fft_size);
        apply_window(fft_input, window_function, _fft_size);
        fft.rfft(fft_input, fft_output, _fft_size);

        // Estimate single-sided periodogram
        // TODO: compute cepstrum
        for (int j = 0; j < (_fft_size >> 1); j++)
        {
            float factor = (j == 0) ? 1 : 2; // make sure to collect power at both sides of the spectrum
            float inst_power = factor * (fft_output[2 * j] * fft_output[2 * j] + fft_output[2 * j + 1] * fft_output[2 * j + 1]) / float(_fft_size);
            periodogram[j] = alpha * periodogram[j] + (1 - alpha) * inst_power; // * FFT_SIZE / SAMPLE_RATE;
        }

        // Find peak in periodogram. Note that the resolution could be improved using a parabola fitting trick
        max_power_density = 0, freq = 0;
        for (int j = 0; j < (_fft_size >> 1); j++)
        {
            if (periodogram[j] > max_power_density)
            {
                max_power_density = 10 * log10(periodogram[j]); // / bin_width);
                freq = float(j) * bin_width;
            }
        }
        take_mutex(mutex);
        voice_pitch_frequency = freq;
        voice_pitch_magnitude = max_power_density;
        give_mutex(mutex);
    }
    return true;
}

bool Pitch::detect_voice_activity()
{
    const float thres_alpha = .9999;
    // todo: should the features be filtered by alow-pass filter or a hampel?

    // estimate signal extremes by fast-attack, slow-decay filtering
    if (avg_power < min_power)
        min_power = avg_power;
    else
        min_power = thres_alpha * min_power + (1 - thres_alpha) * avg_power;
    if (avg_power > max_power)
        max_power = avg_power;
    else
        max_power = thres_alpha * max_power + (1 - thres_alpha) * avg_power;

    // dynamic power thresholding
    float log_avg_power = 10 * log10(avg_power);
    float log_min_power = 10 * log10(min_power);
    float log_max_power = 10 * log10(max_power);
    float thres_lambda = (log_max_power - log_min_power) / log_max_power;
    float power_threshold = (1.0 - thres_lambda) * log_max_power + thres_lambda * log_min_power;
    int feature_0 = log_avg_power > power_threshold;

    // frequency windowing
    int feature_1 = (voice_pitch_frequency > f_c_low) && (voice_pitch_frequency < f_c_high);

    // compute spectral features 
    float num = 0, den = 0;
    for (int j = 0; j < _fft_size; j++)
    {
        float f = (float)j * bin_width;
        if ((f > f_c_low) && (f < f_c_high))
        {
            num += f * periodogram[j];
            den += periodogram[j];
        }
    }
    float spectral_centroid = num/den;
    float num1 = 0, num2 = 0;
    for (int j = 0; j < _fft_size; j++)
    {
        float f = (float)j * bin_width;
        if ((f > f_c_low) && (f < f_c_high))
        {
            float f_n = f - spectral_centroid;
            num1 += pow(f_n, 2) * periodogram[j];
            num2 += pow(f_n, 3) * periodogram[j];
        }
    }
    float spectral_spread_squared = num1 / den;
    float spectral_skewness = num2 / (pow(spectral_centroid, 3) * den);

    voice_active = (feature_0 && feature_1) ? true : false;

    if (feature_0 && feature_1)
    {
        // log_i("min, max, avg, thres power: %f, %f, %f, %f [dB]: %i", log_min_power, log_max_power, log_avg_power, power_threshold, log_avg_power > power_threshold);
        log_i("power density peak: %f [dB/Hz] @ %f [Hz] zero crossing rate: %f [Hz] ", voice_pitch_magnitude, voice_pitch_frequency, zero_crossing_rate);
        take_mutex(mutex);
        voice_active = true;
        give_mutex(mutex);
    }
    return true;
}

void Pitch::i2sSamplerTask(void *param)
{
    Pitch *obj = (Pitch *)param;
    const TickType_t xDelay = 10 / portTICK_PERIOD_MS;
    i2s_config_t _i2sConfigLeftChannel = {
        .mode = (i2s_mode_t)(I2S_MODE_MASTER | I2S_MODE_RX),
        .sample_rate = _sample_rate,
        .bits_per_sample = I2S_BITS_PER_SAMPLE_32BIT,
        .channel_format = I2S_CHANNEL_FMT_ONLY_LEFT,
        .communication_format = i2s_comm_format_t(I2S_COMM_FORMAT_I2S),
        .intr_alloc_flags = ESP_INTR_FLAG_LEVEL1,
        .dma_buf_count = 4,
        .dma_buf_len = 1024,
        .use_apll = false,
        .tx_desc_auto_clear = false,
        .fixed_mclk = 0};
    i2s_pin_config_t _i2sPins = {
        .bck_io_num = obj->_bclkPin,
        .ws_io_num = obj->_lrckPin,
        .data_out_num = I2S_PIN_NO_CHANGE,
        .data_in_num = obj->_dinPin};
    I2SMEMSSampler i2sSampler = I2SMEMSSampler(I2S_NUM_0, _i2sPins, _i2sConfigLeftChannel, true);

    log_d("running on core %d", xPortGetCoreID());
    i2sSampler.start();

    while (true)
    {
        int samples_read = i2sSampler.read(obj->samples, _sample_buffer_size);
        // Note that we should rather have a sample producer task and consumer task to compute periodogram; using dual buffers
        if (samples_read == _sample_buffer_size)
        {
            data d = (data){._buffer = obj->samples, ._size = _sample_buffer_size};
            xQueueSend(obj->dataQueue, &d, (TickType_t)xDelay); // Don't block if the queue is already full.

            obj->preprocess();
            obj->compute_periodogram();
            obj->detect_voice_activity();
        }
    }
}
