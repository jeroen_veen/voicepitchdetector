#pragma once
#include <Arduino.h>

struct data
{
    int16_t *_buffer;
    uint16_t _size;
};