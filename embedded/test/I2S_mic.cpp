// #include <Arduino.h>
// #include "I2S_mic.h"
// #include "soc/i2s_reg.h"


// I2Smic::I2Smic()
// {
//     // Create mutex
//     mutex = xSemaphoreCreateMutex();
//     assert(mutex);
// }


// esp_err_t I2Smic::i2sConfig(int bclkPin, int lrckPin, int dinPin)
// {
//     // i2s configuration: Tx to ext DAC, 2's complement 16-bit PCM, mono,
//     const i2s_config_t i2s_config = {
//         .mode = (i2s_mode_t)(I2S_MODE_MASTER | I2S_MODE_RX), // | I2S_CHANNEL_MONO), 
//         .sample_rate = sample_rate,
//         .bits_per_sample = I2S_BITS_PER_SAMPLE_16BIT,
//         .channel_format = I2S_CHANNEL_FMT_ONLY_LEFT,
//         .communication_format = (i2s_comm_format_t)(I2S_COMM_FORMAT_I2S), // | I2S_COMM_FORMAT_I2S_MSB),
//         .intr_alloc_flags = ESP_INTR_FLAG_LEVEL3, // highest interrupt priority that can be handeled in c
//         .dma_buf_count = 8,                      // 16,
//         .dma_buf_len = 1024,                      //
//         .use_apll = false,
//         .tx_desc_auto_clear = true, // new in V1.0.1
//         .fixed_mclk = -1};

//     const i2s_pin_config_t pin_config = {
//         .bck_io_num = bclkPin,           // this is BCK pin
//         .ws_io_num = lrckPin,            // this is LRCK pin
//         .data_out_num = I2S_PIN_NO_CHANGE, // Not used
//         .data_in_num = dinPin         // this is DATA input pin        
//     };
//     esp_err_t ret1 = i2s_driver_install((i2s_port_t)i2s_num, &i2s_config, 0, NULL);
//     esp_err_t ret2 = i2s_set_pin((i2s_port_t)i2s_num, &pin_config);
//     esp_err_t ret3 = i2s_set_sample_rates((i2s_port_t)i2s_num, sample_rate);

//     return ret1 + ret2 + ret3;
// }

// esp_err_t I2Smic::begin(int bclkPin, int lrckPin, int dinPin)
// {
//     if (i2sConfig(bclkPin, lrckPin, dinPin) == ESP_FAIL)
//     {
//         printf("i2s driver error\n");
//         return ESP_FAIL;
//     }
//     else
//     {
//             // FIXES for SPH0645
//         REG_SET_BIT(I2S_TIMING_REG(m_i2sPort), BIT(9));
//         REG_SET_BIT(I2S_CONF_REG(m_i2sPort), I2S_RX_MSB_SHIFT);
//         printf("i2s driver configured\n");
//         return ESP_OK;
//     }
// }

// esp_err_t I2Smic::read()
// {
//     i2s_read(I2S_NUM, data_recv, ((bits+8)/16)*SAMPLE_PER_CYCLE*4, &i2s_bytes_read, 100);
// }



// int I2SMEMSSampler::read(int16_t *samples, int count)
// {
//     int32_t raw_samples[256];
//     int sample_index = 0;
//     while (count > 0)
//     {
//         size_t bytes_read = 0;
//         i2s_read(m_i2sPort, (void **)raw_samples, sizeof(int32_t) * std::min(count, 256), &bytes_read, portMAX_DELAY);
//         int samples_read = bytes_read / sizeof(int32_t);
//         for (int i = 0; i < samples_read; i++)
//         {
//             samples[sample_index] = (raw_samples[i] & 0xFFFFFFF0) >> 11;
//             sample_index++;
//             count--;
//         }
//     }
//     return sample_index;