#include <TinyPICO.h>
#include "I2S_mic.h"
#include "ESP32_fft.h"
#include <WiFi.h>
#include <HTTPClient.h>
#include "WiFiCredentials.h"
#include "I2SMEMSSampler.h"

const int bclkPin = 27; // I2S Clock
const int lrckPin = 25; // Left/Right audio
const int dinPin = 26; // I2S Data

// global objects
TinyPICO tp = TinyPICO();
WiFiClient *wifiClientI2S = NULL;
HTTPClient *httpClientI2S = NULL;
I2SSampler *i2sSampler = NULL;

// replace this with your machines IP Address
#define I2S_SERVER_URL "http://192.168.1.15:5003/i2s_samples"

// how many samples to read at once
const int SAMPLE_SIZE = 16384;

// i2s config for reading from left channel of I2S
i2s_config_t i2sMemsConfigLeftChannel = {
    .mode = (i2s_mode_t)(I2S_MODE_MASTER | I2S_MODE_RX),
    .sample_rate = 16000,
    .bits_per_sample = I2S_BITS_PER_SAMPLE_32BIT,
    .channel_format = I2S_CHANNEL_FMT_ONLY_LEFT,
    .communication_format = i2s_comm_format_t(I2S_COMM_FORMAT_I2S),
    .intr_alloc_flags = ESP_INTR_FLAG_LEVEL1,
    .dma_buf_count = 4,
    .dma_buf_len = 1024,
    .use_apll = false,
    .tx_desc_auto_clear = false,
    .fixed_mclk = 0};

// i2s pins
i2s_pin_config_t i2sPins = {
    .bck_io_num = bclkPin,
    .ws_io_num = lrckPin,
    .data_out_num = I2S_PIN_NO_CHANGE,
    .data_in_num = dinPin};

// send data to a remote address
void sendData(WiFiClient *wifiClient, HTTPClient *httpClient, const char *url, uint8_t *bytes, size_t count)
{
    // send them off to the server
    digitalWrite(2, HIGH);
    httpClient->begin(*wifiClient, url);
    httpClient->addHeader("content-type", "application/octet-stream");
    httpClient->POST(bytes, count);
    httpClient->end();
    digitalWrite(2, LOW);
}

// task to write samples to our server
void i2sMemsWriterTask(void *param)
{
    I2SSampler *sampler = (I2SSampler *)param;
    int16_t *samples = (int16_t *)malloc(sizeof(uint16_t) * SAMPLE_SIZE);
    if (!samples)
    {
        Serial.println("Failed to allocate memory for samples");
        return;
    }
    while (true)
    {
        int samples_read = sampler->read(samples, SAMPLE_SIZE);
        sendData(wifiClientI2S, httpClientI2S, I2S_SERVER_URL, (uint8_t *)samples, samples_read * sizeof(uint16_t));
    }
}

void setup()
{
    Serial.begin(115200);
    
    // launch WiFi
    Serial.printf("Connecting to WiFi");
    WiFi.mode(WIFI_STA);
    WiFi.begin(SSID, PASSWORD);
    while (WiFi.waitForConnectResult() != WL_CONNECTED)
    {
        Serial.print(".");
        delay(1000);
    }
    Serial.println("");
    Serial.println("WiFi Connected");
    Serial.println("Started up");
    Serial.println("IP address: ");
    Serial.println(WiFi.localIP());
    
    // indicator LED
    pinMode(2, OUTPUT);

    // setup the HTTP Client
    wifiClientI2S = new WiFiClient();
    httpClientI2S = new HTTPClient();

    // direct i2s input from INMP441 or the SPH0645
    i2sSampler = new I2SMEMSSampler(I2S_NUM_0, i2sPins, i2sMemsConfigLeftChannel, true);
    i2sSampler->start();
    
    // set up the i2s sample writer task
    TaskHandle_t i2sMemsSamplerTaskHandle;
    xTaskCreatePinnedToCore(i2sMemsWriterTask, "I2S Writer Task", 4096, i2sSampler, 1, &i2sMemsSamplerTaskHandle, 1);
}

void loop()
{
    // nothing to do here - everything is taken care of by tasks
}
