#ifndef __I2S_MIC__
#define __I2S_MIC__

#include <driver/i2s.h>

class I2Smic
{
private:
  const uint8_t i2s_num = I2S_NUM_0;             // i2s port number
  const uint16_t sample_rate = 16000;            // [Sps]
  SemaphoreHandle_t mutex;

  esp_err_t i2sConfig(int bclkPin, int lrckPin, int dinPin);

public:
  I2Smic();
  esp_err_t begin(int bclkPin, int lrckPin, int dinPin);
};


#endif